/* GConf.xs:
 *
 * Copyright (C) 2002 Mark McLoughlin
 * Copyright (C) 2002 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include <gconf/gconf-schema.h>

#define GCONF_XS_DEBUG

#ifndef GCONF_XS_DEBUG
#define dprintf(...)
#else
#define dprintf(...) fprintf (stderr, __VA_ARGS__)
#endif

typedef GConfClient *GConf__Client;
typedef GConfValue  *GConf__Value;
typedef GConfEntry  *GConf__Entry;
typedef GConfSchema *GConf__Schema;
typedef GMainLoop   *GConf__MainLoop;

typedef struct {
	SV *sub;
	SV *data;
} PClosure;

/* FIXME: we need this made public in GConf */
void gconf_entry_ref (GConfEntry *entry);

static inline void
sv_set_g_error (SV     *sv_error,
	        GError *g_error)
{
	sv_setpv (sv_error, g_error->message);
	g_error_free (g_error);
}

static GConfClientPreloadType
string_to_preload_type (const char *type)
{
	GConfClientPreloadType retval;

	retval = GCONF_CLIENT_PRELOAD_NONE;

	if (!type)
		return retval;

	switch (type [0]) {
	case 'n':
		if (!strcmp (type, "none"))
			retval = GCONF_CLIENT_PRELOAD_NONE;
		break;
	case 'o':
		if (!strcmp (type, "onelevel"))
			retval = GCONF_CLIENT_PRELOAD_ONELEVEL;
		break;
	case 'r':
		if (!strcmp (type, "recursive"))
			retval = GCONF_CLIENT_PRELOAD_RECURSIVE;
		break;
	default:
		break;
	}

	return retval;
}

static GConfValueType
string_to_value_type (const char *type)
{
	GConfValueType vtype;

	vtype = GCONF_VALUE_INVALID;

	if (!type)
		return vtype;

	switch (type [0]) {
	case 's':
		if (!strcmp (type, "string"))
			vtype = GCONF_VALUE_STRING;
		else if (!strcmp (type, "schema"))
			vtype = GCONF_VALUE_SCHEMA;
		break;
	case 'i':
		if (!strcmp (type, "int"))
			vtype = GCONF_VALUE_INT;
		if (!strcmp (type, "invalid"))
			vtype = GCONF_VALUE_INVALID;
		break;
	case 'f':
		if (!strcmp (type, "float"))
			vtype = GCONF_VALUE_FLOAT;
		break;
	case 'b':
		if (!strcmp (type, "bool"))
			vtype = GCONF_VALUE_BOOL;
		break;
	case 'l':
		if (!strcmp (type, "list"))
			vtype = GCONF_VALUE_LIST;
		break;
	case 'p':
		if (!strcmp (type, "pair"))
			vtype = GCONF_VALUE_PAIR;
		break;
	default:
		break;
	}

	if (vtype == -1)
		croak ("unkown GConf type '%s'", type);

	return vtype;
}

static G_CONST_RETURN char *
value_type_to_string (GConfValueType vtype)
{
	switch (vtype) {
	case GCONF_VALUE_INVALID:
		return "invalid";
	case GCONF_VALUE_STRING:
		return "string";
	case GCONF_VALUE_INT:
		return "int";
	case GCONF_VALUE_FLOAT:
		return "float";
	case GCONF_VALUE_BOOL:
		return "bool";
	case GCONF_VALUE_SCHEMA:
		return "schema";
	case GCONF_VALUE_PAIR:
		return "pair";
	case GCONF_VALUE_LIST:
		return "list";
	default:
		g_assert_not_reached ();
		break;
	}

	return NULL;
}

static GConfValue *
value_from_sv (SV             *scalar,
	       GConfValueType  vtype)
{
	GConfValue *retval;

	retval = gconf_value_new (vtype);

	switch (vtype) {
	case GCONF_VALUE_STRING:
		gconf_value_set_string (retval, SvPV (scalar, PL_na));
		break;
	case GCONF_VALUE_INT:
		gconf_value_set_int (retval, SvIV (scalar));
		break;
	case GCONF_VALUE_FLOAT:
		gconf_value_set_float (retval, SvNV (scalar));
		break;
	default:
		croak ("GConf::value_from_sv: handling '%s' NYI",
		       value_type_to_string (vtype));
		break;
	}

	return retval;
}

static SV *
sv_from_value (GConfValue *value)
{
	SV *retval = NULL;

	switch (value->type) {
	case GCONF_VALUE_STRING:
		retval = newSVpv (gconf_value_get_string (value), 0);
		break;
	case GCONF_VALUE_INT:
		retval = newSViv (gconf_value_get_int (value));
		break;
	case GCONF_VALUE_FLOAT:
		retval = newSVnv (gconf_value_get_float (value));
		break;
	case GCONF_VALUE_BOOL:
	case GCONF_VALUE_INVALID:
	case GCONF_VALUE_SCHEMA:
		croak ("GConf::sv_from_value: handling '%s' NYI",
		       value_type_to_string (value->type));
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	return retval;
}

static void
set_value_from_stack (GConfValue     *value,
		      GConfValueType  vtype)
{
	dXSARGS;

	if (items < 1)
		return;

	switch (vtype) {
	case GCONF_VALUE_STRING:
		gconf_value_set_string (value, SvPV (ST (0), PL_na));
		break;
	case GCONF_VALUE_INT:
		gconf_value_set_int (value, SvIV (ST (0)));
		break;
	case GCONF_VALUE_BOOL:
		gconf_value_set_bool (value, SvIV (ST (0)));
		break;
	case GCONF_VALUE_FLOAT:
		gconf_value_set_float (value, SvNV (ST (0)));
		break;
	case GCONF_VALUE_SCHEMA:
		if (sv_derived_from (ST (0), "GConf::Schema")) {
			IV tmp = SvIV ((SV*) SvRV (ST (0)));
			gconf_value_set_schema (value, INT2PTR (GConfSchema *, tmp));
		} else
			croak ("argument 0 is not of type GConf::Schema");
		break;
	case GCONF_VALUE_LIST: {
		GConfValueType  list_type = string_to_value_type (SvPV (ST (0), PL_na));
		GSList         *list = NULL;
		int             i;

		gconf_value_set_list_type (value, list_type);

		for (i = items - 1; i >= 1; i--)
			list = g_slist_prepend (
					list, value_from_sv (ST (i), list_type));

		if (list)
			gconf_value_set_list_nocopy (value, list);
		}
		break;
	case GCONF_VALUE_PAIR: {
		GConfValueType car_type;
		GConfValueType cdr_type;

		if (items < 4)
			croak ("trying to set a pair value without two values\n");

		car_type = string_to_value_type (SvPV (ST (0), PL_na));
		cdr_type = string_to_value_type (SvPV (ST (2), PL_na));

		gconf_value_set_car_nocopy (
			value, value_from_sv (ST (1), car_type));
		gconf_value_set_cdr_nocopy (
			value, value_from_sv (ST (3), cdr_type));
		}
		break;
	case GCONF_VALUE_INVALID:
	default:
		break;
	}
}

static void
client_notify_handler (GConfClient *client,
		       guint        cnxn_id,
		       GConfEntry  *entry,
		       gpointer     user_data)
{
	dSP;

	PClosure *closure = user_data;
	int       n_ret;
	SV       *client_sv;
	SV       *entry_sv;

	ENTER;
	SAVETMPS;

	client_sv = sv_newmortal ();
	sv_setref_pv (client_sv, "GConf::Client", g_object_ref (client));

	entry_sv = sv_newmortal ();
	gconf_entry_ref (entry);
	sv_setref_pv (entry_sv, "GConf::Entry", entry);

	PUSHMARK (SP);
	EXTEND (SP, 4);
	PUSHs (client_sv);
	PUSHs (sv_2mortal (newSViv (cnxn_id)));
	PUSHs (entry_sv);
	PUSHs (sv_2mortal (closure->data));
	PUTBACK;

	n_ret = call_sv (closure->sub, G_SCALAR | G_DISCARD);
	if (n_ret)
		croak ("GConf::client_notify_handler: notify handler returned %d values", n_ret);

	FREETMPS;
	LEAVE;
}

static PClosure *
pclosure_new (SV *sub, SV *data)
{
	PClosure *retval;

	retval = g_new (PClosure, 1);

	retval->sub  = SvREFCNT_inc (sub);
	retval->data = SvREFCNT_inc (data);

	return retval;
}

static void
pclosure_free (PClosure *closure)
{
	SvREFCNT_dec (closure->sub);
	SvREFCNT_dec (closure->data);

	g_free (closure);
}
	      
MODULE = GConf          PACKAGE = GConf

void
setup_mem_vtable ()
  CODE:
#ifdef GCONF_XS_DEBUG
	g_mem_set_vtable (glib_mem_profiler_table);
#endif

MODULE = GConf		PACKAGE = GConf::Client

GConf::Client
new (Class)
  CODE:
	RETVAL = gconf_client_get_default ();
  OUTPUT:
	RETVAL

void
DESTROY (self)
	GConf::Client self;
  CODE:
	g_object_unref (self);

void
add_dir (self, dir, preload, ...)
	GConf::Client  self;
	char          *dir;
	char          *preload;
  ALIAS:
	GConf::Client::add_dir = 0
	GConf::Client::preload = 1
  CODE:
    {
	GConfClientPreloadType   preload_type;
	GError                  *error = NULL;
	GError                 **err = NULL;

	preload_type = string_to_preload_type (preload);

	if (items > 3)
		err = &error;

	switch (ix) {
	case 0:
		gconf_client_add_dir (self, dir, preload_type, err);
		break;
	case 1:
		gconf_client_preload (self, dir, preload_type, err);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	if (err && error)
		sv_set_g_error (ST (3), error);
    }

void
remove_dir (self, dir, ...)
	GConf::Client  self;
	char          *dir;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	gconf_client_remove_dir (self, dir, err);
	if (err && error)
		sv_set_g_error (ST (2), error);
    }

unsigned int
notify_add (self, namespace_section, handler, data, ...)
	GConf::Client  self;
	char          *namespace_section;
	SV            *handler;
	SV            *data;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 4)
		err = &error;

	RETVAL = gconf_client_notify_add (
			self, namespace_section,
			client_notify_handler,
			pclosure_new (handler, data),
			(GDestroyNotify) pclosure_free,
			err);

	if (err && error)
		sv_set_g_error (ST (4), error);
    }
  OUTPUT:
	RETVAL

bool
associate_schema (self, key, schema_name, ...)
	GConf::Client  self;
	char          *key;
	char          *schema_name
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 3)
		err = &error;

	RETVAL = gconf_engine_associate_schema (
			self->engine, key, schema_name, err);
	if (err && error)
		sv_set_g_error (ST (3), error);
    }
  OUTPUT:
	RETVAL

void
set (self, key, value, ...)
	GConf::Client  self;
	char          *key;
	GConf::Value   value;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 3)
		err = &error;

	gconf_client_set (self, key, value, err);
	if (err && error)
		sv_set_g_error (ST (3), error);
    }

GConf::Value
get (self, key, ...)
	GConf::Client  self;
	char          *key;
  ALIAS:
	GConf::Client::get = 0
	GConf::Client::get_without_default = 1
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	switch (ix) {
	case 0:
		RETVAL = gconf_client_get (self, key, err);
		break;
	case 1:
		RETVAL = gconf_client_get_without_default (self, key, err);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	if (err && error) {
		g_assert (RETVAL == NULL);
		sv_set_g_error (ST (2), error);
	}
    }
  OUTPUT:
	RETVAL

GConf::Entry
get_entry (self, key, locale, use_default, ...)
	GConf::Client  self;
	char          *key;
	char          *locale;
	bool           use_default;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 4)
		err = &error;

	RETVAL = gconf_client_get_entry (
			self, key, locale, use_default, err);
	if (err && error) {
		g_assert (RETVAL == NULL);
		sv_set_g_error (ST (4), error);
	}
    }
  OUTPUT:
	RETVAL

GConf::Value
get_default_from_schema (self, key, ...)
	GConf::Client  self;
	char          *key;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	RETVAL = gconf_client_get_default_from_schema (self, key, err);
	if (err && error) {
		g_assert (RETVAL == NULL);
		sv_set_g_error (ST (2), error);
	}
    }
  OUTPUT:
	RETVAL

bool
unset (self, key, ...)
	GConf::Client  self;
	char          *key;
  ALIAS:
	GConf::Client::unset = 0
	GConf::Client::key_is_writable = 1
	GConf::Client::dir_exists = 2
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	switch (ix) {
	case 0:
		RETVAL = gconf_client_unset (self, key, err);
		break;
	case 1:
		RETVAL = gconf_client_key_is_writable (self, key, err);
		break;
	case 2:
		RETVAL = gconf_client_dir_exists (self, key, err);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
		
	if (err && error)
		sv_set_g_error (ST (2), error);
    }
  OUTPUT:
	RETVAL

void
suggest_sync (self, ...)
	GConf::Client self;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 1)
		err = &error;

	gconf_client_suggest_sync (self, err);
	if (err && error)
		sv_set_g_error (ST (1), error);
    }

void
clear_cache (self)
	GConf::Client self;
  CODE:
	gconf_client_clear_cache (self);

void
all_entries (self, dir, ...)
	GConf::Client  self;
	char          *dir;
  ALIAS:
	GConf::Client::all_entries = 0
	GConf::Client::all_dirs = 1
  PPCODE:
    {
	GError  *error = NULL;
	GError **err = NULL;
	GSList  *list, *l;

	if (items > 2)
		err = &error;

	switch (ix) {
	case 0:
		list = gconf_client_all_entries (self, dir, err);
		break;
	case 1:
		list = gconf_client_all_dirs (self, dir, err);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	if (err && error) {
		g_assert (list == NULL);
		sv_set_g_error (ST (2), error);
		XSRETURN_EMPTY;
	}

	EXTEND (SP, g_slist_length (list));

	for (l = list; l; l = l->next) {
		SV *sv_entry;

		switch (ix) {
		case 0:
			sv_entry = sv_newmortal ();
			sv_setref_pv (sv_entry, "GConf::Entry", l->data);
			PUSHs (sv_entry);
			break;
		case 1:
			PUSHs (sv_2mortal (newSVpv (l->data, 0)));
			g_free (l->data);
			break;
		default:
			g_assert_not_reached ();
			break;
		}
	}

	g_slist_free (list);
    }

bool
set_string (self, key, the_string, ...)
	GConf::Client  self;
	char          *key;
	char          *the_string;
  CODE:
    {
	GError      *error = NULL;
	GError     **err = NULL;
	GConfValue  *retval;

	if (items > 3)
		err = &error;

	RETVAL = gconf_client_set_string (
			self, key, the_string, err);
	if (!RETVAL && err && error)
		sv_set_g_error (ST (3), error);
    }
  OUTPUT:
	RETVAL

char *
get_string (self, key, ...)
	GConf::Client  self;
	char          *key;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;
	char    *str;

	if (items > 2)
		err = &error;

	RETVAL = gconf_client_get_string (self, key, err);
	if (err && error) {
		g_assert (RETVAL == NULL);
		sv_set_g_error (ST (2), error);
	}
    }
  OUTPUT:
	RETVAL
  CLEANUP:
	g_free (RETVAL);

bool
set_int (self, key, the_int, ...)
	GConf::Client  self;
	char          *key;
	int            the_int;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 3)
		err = &error;

	RETVAL = gconf_client_set_int (self, key, the_int, err);
	if (!RETVAL && err && error)
		sv_set_g_error (ST (3), error);
    }
  OUTPUT:
	RETVAL

int
get_int (self, key, ...)
	GConf::Client  self;
	char          *key;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	RETVAL = gconf_client_get_int (self, key, err);
	if (err && error)
		sv_set_g_error (ST (2), error);
    }
  OUTPUT:
	RETVAL

bool
set_float (self, key, the_float, ...)
	GConf::Client  self;
	char          *key;
	double         the_float;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 3)
		err = &error;

	RETVAL = gconf_client_set_float (self, key, the_float, err);
	if (!RETVAL && err && error)
		sv_set_g_error (ST (3), error);
    }
  OUTPUT:
	RETVAL

double
get_float (self, key, ...)
	GConf::Client  self;
	char          *key;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	RETVAL = gconf_client_get_float (self, key, err);
	if (err && error)
		sv_set_g_error (ST (2), error);
    }
  OUTPUT:
	RETVAL

bool
set_bool (self, key, the_bool, ...)
	GConf::Client  self;
	char          *key;
	bool           the_bool;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 3)
		err = &error;

	RETVAL = gconf_client_set_bool (self, key, the_bool, err);
	if (!RETVAL && err && error)
		sv_set_g_error (ST (3), error);
    }
  OUTPUT:
	RETVAL

bool
get_bool (self, key, ...)
	GConf::Client  self;
	char          *key;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	RETVAL = gconf_client_get_bool (self, key, err);
	if (err && error)
		sv_set_g_error (ST (2), error);
    }
  OUTPUT:
	RETVAL

bool
set_schema (self, key, the_schema, ...)
	GConf::Client  self;
	char          *key;
	GConf::Schema  the_schema;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 3)
		err = &error;

	RETVAL = gconf_client_set_schema (
			self, key, the_schema, err);
	if (!RETVAL && err && error)
		sv_set_g_error (ST (3), error);
    }
  OUTPUT:
	RETVAL

GConf::Schema
get_schema (self, key, ...)
	GConf::Client  self;
	char          *key;
  CODE:
    {
	GError  *error = NULL;
	GError **err = NULL;

	if (items > 2)
		err = &error;

	RETVAL = gconf_client_get_schema (self, key, err);
	if (err && error) {
		g_assert (RETVAL == NULL);
		sv_set_g_error (ST (2), error);
	}
    }
  OUTPUT:
	RETVAL

bool
set_list (self, key, error, type, ...)
	GConf::Client  self;
	char          *key;
	char          *type;
  CODE:
    {
	GConfValueType  vtype = string_to_value_type (type);
	GConfValue     *value;
	GError         *error = NULL;
	GSList         *list = NULL;
	int             i;

	RETVAL = TRUE;

	switch (vtype) {
	case GCONF_VALUE_BOOL:
	case GCONF_VALUE_SCHEMA:
	case GCONF_VALUE_INVALID:
		croak ("GConf::Client::set_list: handling '%s' NYI", type);
		break;
	case GCONF_VALUE_LIST:
	case GCONF_VALUE_PAIR:
		croak ("GConf::Client:set_list: you may not have lists of '%s'", type);
		break;
	default:
		break;
	}

	value = gconf_value_new (GCONF_VALUE_LIST);
	gconf_value_set_list_type (value, vtype);

	for (i = items - 1; i >= 4; i--)
		list = g_slist_prepend (
				list, value_from_sv (ST (i), vtype));

	if (list)
		gconf_value_set_list_nocopy (value, list);

	gconf_client_set (self, key, value, &error);
	if (error) {
		sv_set_g_error (ST (2), error);
		RETVAL = FALSE;
	}

	gconf_value_free (value);
    }
  OUTPUT:
	RETVAL

void
get_list (self, key, ...)
	GConf::Client  self;
	char          *key;
  PPCODE:
    {
	GConfValue  *value;
	GSList      *list, *l;
	GError      *error = NULL;
	GError     **err = NULL;

	if (items > 2)
		err = &error;

	value = gconf_client_get (self, key, err);
	if (err && error) {
		g_assert (value == NULL);
		sv_set_g_error (ST (2), error);
		XSRETURN_EMPTY;

	} else if (!value)
		XSRETURN_EMPTY;

	list = gconf_value_get_list (value);

	EXTEND (SP, g_slist_length (list));

	for (l = list; l; l = l->next)
		PUSHs (sv_2mortal (sv_from_value (l->data)));

	gconf_value_free (value);
    }

bool
set_pair (self, key, car_type, car, cdr_type, cdr, ...)
	GConf::Client  self;
	char          *key;
	char          *car_type;
	char          *cdr_type;
  CODE:
    {
	GConfValueType   car_vtype = string_to_value_type (car_type);
	GConfValueType   cdr_vtype = string_to_value_type (cdr_type);
	GConfValue      *value;
	GError          *error = NULL;
	GError         **err = NULL;

	RETVAL = TRUE;

	if (items > 6)
		err = &error;

	value = gconf_value_new (GCONF_VALUE_PAIR);

	gconf_value_set_car_nocopy (
		value, value_from_sv (ST (3), car_vtype));
	gconf_value_set_cdr_nocopy (
		value, value_from_sv (ST (5), cdr_vtype));

	gconf_client_set (self, key, value, err);
	if (err && error) {
		sv_set_g_error (ST (6), error);
		RETVAL = FALSE;
	}

	gconf_value_free (value);
    }
  OUTPUT:
	RETVAL

void
get_pair (self, key, ...)
	GConf::Client  self;
	char          *key;
  PPCODE:
    {
	GConfValue  *value;
	GError      *error = NULL;
	GError     **err = NULL;

	if (items > 2)
		err = &error;

	value = gconf_client_get (self, key, err);
	if (err && error) {
		g_assert (value == NULL);
		sv_set_g_error (ST (2), error);
		XSRETURN_EMPTY;

	} else if (!value)
		XSRETURN_EMPTY;

	EXTEND (SP, 2);
	PUSHs (sv_2mortal (sv_from_value (gconf_value_get_car (value))));
	PUSHs (sv_2mortal (sv_from_value (gconf_value_get_cdr (value))));

	gconf_value_free (value);
    }

MODULE = GConf		PACKAGE = GConf::Value

GConf::Value
new (Class, ...)
  CODE:
    {
	GConfValueType vtype = GCONF_VALUE_INVALID;

	if (items > 1)
		vtype = string_to_value_type (SvPV (ST (1), PL_na));

	RETVAL = gconf_value_new (vtype);

	PUSHMARK (MARK + 2);
	set_value_from_stack (RETVAL, vtype);
    }
  OUTPUT:
	RETVAL

void
DESTROY (self)
	GConf::Value self;
  CODE:
	gconf_value_free (self);

char *
get_type (self)
	GConf::Value self
  CODE:
	RETVAL = (char *) value_type_to_string (self->type);
  OUTPUT:
	RETVAL

char *
stringify (self)
	GConf::Value self
  CODE:
	RETVAL = gconf_value_to_string (self);
  OUTPUT:
	RETVAL
  CLEANUP:
	g_free (RETVAL);

void
set_value (self, ...)
	GConf::Value self;
  ALIAS:
	GConf::Value::set_int = 0
	GConf::Value::set_string = 1
	GConf::Value::set_float = 2
	GConf::Value::set_bool = 3
	GConf::Value::set_schema = 4
	GConf::Value::set_list = 5
	GConf::Value::set_pair = 6
  CODE:
    {
	GConfValueType type = GCONF_VALUE_INVALID;

	switch (ix) {
	case 0:
		type = GCONF_VALUE_INT;
		break;
	case 1:
		type = GCONF_VALUE_STRING;
		break;
	case 2:
		type = GCONF_VALUE_FLOAT;
		break;
	case 3:
		type = GCONF_VALUE_BOOL;
		break;
	case 4:
		type = GCONF_VALUE_SCHEMA;
		break;
	case 5:
		type = GCONF_VALUE_LIST;
		break;
	case 6:
		type = GCONF_VALUE_PAIR;
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	PUSHMARK (MARK + 1);
	set_value_from_stack (self, type);
    }

int
get_int (self)
	GConf::Value self;
  CODE:
	RETVAL = gconf_value_get_int (self);
  OUTPUT:
	RETVAL

char *
get_string (self)
	GConf::Value self;
  CODE:
	RETVAL = (char *) gconf_value_get_string (self);
  OUTPUT:
	RETVAL

double
get_float (self)
	GConf::Value self;
  CODE:
	RETVAL = gconf_value_get_float (self);
  OUTPUT:
	RETVAL

bool
get_bool (self)
	GConf::Value self;
  CODE:
	RETVAL = gconf_value_get_bool (self);
  OUTPUT:
	RETVAL

GConf::Schema
get_schema (self)
	GConf::Value self
  CODE:
	RETVAL = gconf_value_get_schema (self);
  OUTPUT:
	RETVAL

char *
get_list_type (self)
	GConf::Value self
  CODE:
	RETVAL = (char *) value_type_to_string (
				gconf_value_get_list_type (self));
  OUTPUT:
	RETVAL

void
get_list (self)
	GConf::Value self;
  PPCODE:
    {
	GSList *list;

	list = gconf_value_get_list (self);

	EXTEND (SP, g_slist_length (list));

	for (; list; list = list->next)
		PUSHs (sv_2mortal (sv_from_value (list->data)));
    }

void
get_pair (self)
	GConf::Value self;
  PPCODE:
    {
	EXTEND (SP, 2);
	PUSHs (sv_2mortal (sv_from_value (gconf_value_get_car (self))));
	PUSHs (sv_2mortal (sv_from_value (gconf_value_get_cdr (self))));
    }

GConf::Value
get_car (self)
	GConf::Value self;
  CODE:
	RETVAL = gconf_value_copy (
			gconf_value_get_car (self));
  OUTPUT:
	RETVAL

GConf::Value
get_cdr (self)
	GConf::Value self;
  CODE:
	RETVAL = gconf_value_copy (
			gconf_value_get_cdr (self));
  OUTPUT:
	RETVAL

MODULE = GConf		PACKAGE = GConf::Entry

GConf::Entry
new (Class, key, value)
	char         *key;
	GConf::Value  value;
  CODE:
	RETVAL = gconf_entry_new (key, value);
  OUTPUT:
	RETVAL

void
DESTROY (self)
	GConf::Entry self;
  CODE:
	gconf_entry_free (self);

char *
get_key (self)
	GConf::Entry self;
  CODE:
	RETVAL = (char *) gconf_entry_get_key (self);
  OUTPUT:
	RETVAL

GConf::Value
get_value (self)
	GConf::Entry self;
  CODE:
	RETVAL = gconf_value_copy (
			gconf_entry_get_value (self));
  OUTPUT:
	RETVAL

char *
get_schema_name (self)
	GConf::Entry self;
  CODE:
	RETVAL = (char *) gconf_entry_get_schema_name (self);
  OUTPUT:
	RETVAL

bool
is_default (self)
	GConf::Entry self;
  CODE:
	RETVAL = gconf_entry_get_is_default (self);
  OUTPUT:
	RETVAL

bool
is_writable (self)
	GConf::Entry self;
  CODE:
	RETVAL = gconf_entry_get_is_writable (self);
  OUTPUT:
	RETVAL

void
set_value (self, value)
	GConf::Entry self;
	GConf::Value value;
  CODE:
	gconf_entry_set_value (self, value);

void
set_schema_name (self, name)
	GConf::Entry  self;
	char       *name;
  CODE:
	gconf_entry_set_schema_name (self, name);

void
set_is_default (self, is_default)
	GConf::Entry self;
	bool         is_default;
  CODE:
	gconf_entry_set_is_default (self, is_default);

void
set_is_writable (self, is_writable)
	GConf::Entry self;
	bool         is_writable;
  CODE:
	gconf_entry_set_is_writable (self, is_writable);

MODULE = GConf		PACKAGE = GConf::Schema

GConf::Schema
new (Class)
  CODE:
	RETVAL = gconf_schema_new ();
  OUTPUT:
	RETVAL

void
DESTROY (self)
	GConf::Schema self;
  CODE:
	gconf_schema_free (self);

void
set_type (self, type)
	GConf::Schema  self;
	char          *type;
  CODE:
	gconf_schema_set_type (self, string_to_value_type (type));

void
set_list_type (self, list_type)
	GConf::Schema  self;
	char          *list_type;
  CODE:
	gconf_schema_set_list_type (
		self, string_to_value_type (list_type));

void
set_car_type (self, car_type)
	GConf::Schema  self;
	char          *car_type;
  CODE:
	gconf_schema_set_car_type (
		self, string_to_value_type (car_type));

void
set_cdr_type (self, cdr_type)
	GConf::Schema  self;
	char          *cdr_type;
  CODE:
	gconf_schema_set_cdr_type (
		self, string_to_value_type (cdr_type));

void
set_locale (self, locale)
	GConf::Schema  self;
	char          *locale;
  CODE:
	gconf_schema_set_locale (self, locale);

void
set_short_desc (self, desc)
	GConf::Schema  self;
	char          *desc;
  CODE:
	gconf_schema_set_short_desc (self, desc);

void
set_long_desc (self, desc)
	GConf::Schema  self;
	char          *desc;
  CODE:
	gconf_schema_set_long_desc (self, desc);

void
set_owner (self, owner)
	GConf::Schema  self;
	char          *owner;
  CODE:
	gconf_schema_set_owner (self, owner);

void
set_default (self, value)
	GConf::Schema self;
	GConf::Value  value;
  CODE:
	gconf_schema_set_default_value (self, value);

char *
get_type (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) value_type_to_string (
				gconf_schema_get_type (self));
  OUTPUT:
	RETVAL

char *
get_list_type (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) value_type_to_string (
				gconf_schema_get_list_type (self));
  OUTPUT:
	RETVAL

char *
get_car_type (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) value_type_to_string (
				gconf_schema_get_car_type (self));
  OUTPUT:
	RETVAL

char *
get_cdr_type (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) value_type_to_string (
				gconf_schema_get_cdr_type (self));
  OUTPUT:
	RETVAL

char *
get_locale (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) gconf_schema_get_locale (self);
  OUTPUT:
	RETVAL

char *
get_short_desc (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) gconf_schema_get_short_desc (self);
  OUTPUT:
	RETVAL

char *
get_long_desc (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) gconf_schema_get_long_desc (self);
  OUTPUT:
	RETVAL

char *
get_owner (self)
	GConf::Schema self;
  CODE:
	RETVAL = (char *) gconf_schema_get_owner (self);
  OUTPUT:
	RETVAL

GConf::Value
get_default (self)
	GConf::Schema self;
  CODE:
	RETVAL = gconf_value_copy (
			gconf_schema_get_default_value (self));
  OUTPUT:
	RETVAL

MODULE = GConf		PACKAGE = GConf::MainLoop

GConf::MainLoop
new (Class)
  CODE:
    {
	RETVAL = g_main_loop_new (NULL, TRUE);
    }
  OUTPUT:
	RETVAL

void
DESTROY (self)
	GConf::MainLoop self;
  CODE:
    {
	g_main_loop_unref (self);
    }

void
run (self)
	GConf::MainLoop self;
  CODE:
    {
	g_main_loop_run (self);
    }

void
quit (self)
	GConf::MainLoop self;
  CODE:
    {
	g_main_loop_quit (self);
    }

void
iteration (self, may_block)
	GConf::MainLoop self;
	bool            may_block;
  CODE:
    {
	g_main_context_iteration (
		g_main_loop_get_context (self), may_block);
    }

bool
pending (self)
	GConf::MainLoop self;
  CODE:
    {
	RETVAL = g_main_context_pending (
			g_main_loop_get_context (self));
    }
  OUTPUT:
	RETVAL
