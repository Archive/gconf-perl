package GConf;

use warnings;
use strict;
use vars qw($VERSION @ISA);

require DynaLoader;

@ISA = qw(DynaLoader);

$VERSION = '0.3.0';

bootstrap GConf $VERSION;

sub import () {
    my $pkg = shift ();

    GConf::setup_mem_vtable ();
}

1;
