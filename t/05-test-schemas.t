#!/usr/bin/perl -w

BEGIN { $| = 1; print "1..9\n\n"; }
END {print "not ok  1\n" unless $loaded;}

use strict;
use vars qw($loaded);
use ExtUtils::testlib;
use GConf;

$loaded = 1;
print "ok  1\n";

my ($error, $schema, $default, $val);
my @list;
my $client = new GConf::Client ();

$client->unset ("/tmp/test-schema", $error);
print !$error ? "ok  2\n" : "not ok  2\n";

$client->unset ("/schemas/tmp/test-schema", $error);
print !$error ? "ok  3\n" : "not ok  3\n";

$schema = new GConf::Schema ();
$schema->set_locale ("C");
$schema->set_short_desc ("A test schema");
$schema->set_long_desc ("This is only a test schema so I couldn't be bothered writing anything long here");
$schema->set_owner ("gconf-perl");
$schema->set_type ("list");
$schema->set_list_type ("int");

$default = new GConf::Value ("list", "int", 1, 2, 3);

$schema->set_default ($default);

$client->set_schema ("/schemas/tmp/test-schema", $schema, $error);
print !$error ? "ok  4\n" : "not ok  4\n";

$client->associate_schema ("/tmp/test-schema", "/schemas/tmp/test-schema", $error);
print !$error ? "ok  5\n" : "not ok  5\n";

$val = $client->get ("/tmp/test-schema", $error);
print !$error ? "ok  6\n" : "not ok  6\n";

print $val->get_type eq "list" ? "ok  7\n" : "not ok  7\n";
print $val->get_list_type eq "int" ? "ok  8\n" : "not ok  8\n";

@list = $val->get_list ();
print $list[0] == 1 && $list[1] == 2 && $list[2] == 3 ? "ok  9\n" : "not ok  9\n";

