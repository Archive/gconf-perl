#!/usr/bin/perl -w

BEGIN { $| = 1; print "1..10\n\n"; }
END {print "not ok  1\n" unless $loaded;}

use strict;
use vars qw($loaded);
use ExtUtils::testlib;
use GConf;

$loaded = 1;
print "ok  1\n";

my $error;
my $client = new GConf::Client ();

my @list = ("once", "upon", "a", "time");

$client->set_list ("/tmp/test-list", $error, "string", @list);
print !$error ? "ok  2\n" : "not ok  2\n";

@list = $client->get_list ("/tmp/test-list", $error);
print !$error ? "ok  3\n" : "not ok  3\n";
print $list [0] eq "once" &&
      $list [1] eq "upon" &&
      $list [2] eq "a" &&
      $list [3] eq "time" ? "ok  4\n" : "not ok  4\n";

@list = (3, 5, 8, 123);

$client->set_list ("/tmp/test-list", $error, "int", @list);
print !$error ? "ok  5\n" : "not ok  5\n";

@list = $client->get_list ("/tmp/test-list", $error);
print !$error ? "ok  6\n" : "not ok  6\n";
print $list [0] == 3 &&
      $list [1] == 5 &&
      $list [2] == 8 &&
      $list [3] == 123 ? "ok  7\n" : "not ok  7\n";

$client->set_pair ("/tmp/test-pair", "string", "blah", "int", 10, $error);
print !$error ? "ok  8\n" : "not ok  8\n";

my ($car, $cdr) = $client->get_pair ("/tmp/test-pair", $error);
print !$error ? "ok  9\n" : "not ok  9\n";
print $car eq "blah" && $cdr == 10 ? "ok 10\n" : "not ok 10\n";
