#!/usr/bin/perl -w

BEGIN { $| = 1; print "1..7\n\n"; }
END {print "not ok  1\n" unless $loaded;}

use strict;
use vars qw($loaded $got_notify);
use ExtUtils::testlib;
use GConf;

$loaded = 1;
print "ok  1\n";

my $error;
my $client = new GConf::Client ();
my $loop = new GConf::MainLoop ();

$client->add_dir ("/tmp", "recursive", $error);
print !$error ? "ok  2\n" : "not ok  2\n";

$client->notify_add ("/tmp", sub { $got_notify=1; $_[3]->quit (); }, $loop, $error);
print !$error ? "ok  3\n" : "not ok  3\n";

while ($loop->pending ()) {
        $loop->iteration ();
}

$client->set_string ("/tmp/blah-de-blah", scalar (gmtime ()), $error);
print !$error ? "ok  4\n" : "not ok  4\n";

while (!$loop->pending ()) {
        sleep (1);
}

$loop->run ();

print $got_notify ? "ok  5\n" : "not ok  5\n";

$client->remove_dir ("/tmp", $error);
print !$error ? "ok  6\n" : "not ok  6\n";

$client->preload ("/", "recursive", $error);
print !$error ? "ok  7\n" : "not ok  7\n";
