#!/usr/bin/perl -w

BEGIN { $| = 1; print "1..19\n\n"; }
END {print "not ok  1\n" unless $loaded;}

use strict;
use vars qw($loaded);
use ExtUtils::testlib;
use GConf;

$loaded = 1;
print "ok  1\n";

my ($val1, $val2);
my (@list, @pair);

$val1 = new GConf::Value ("int");
$val1->set_int (111);
print $val1->get_int () == 111 ? "ok  2\n" : "not ok  2\n";

$val1 = new GConf::Value ("int", 111);
print $val1->get_int () == 111 ? "ok  3\n" : "not ok  3\n";

$val1 = new GConf::Value ("string");
$val1->set_string ("blah blah blah blah");
print $val1->get_string () eq "blah blah blah blah" ? "ok  4\n" : "not ok  4\n";

$val1 = new GConf::Value ("string", "blah blah blah blah");
print $val1->get_string () eq "blah blah blah blah" ? "ok  5\n" : "not ok  5\n";

$val1 = new GConf::Value ("float");
$val1->set_float (1.359);
print $val1->get_float () == 1.359 ? "ok  6\n" : "not ok  6\n";

$val1 = new GConf::Value ("float", 1.359);
print $val1->get_float () == 1.359 ? "ok  7\n" : "not ok  7\n";

$val1 = new GConf::Value ("bool");
$val1->set_bool (1);
print $val1->get_bool () ? "ok  8\n" : "not ok  8\n";

$val1 = new GConf::Value ("bool", 1);
print $val1->get_bool () ? "ok  9\n" : "not ok  9\n";

$val1 = new GConf::Value ("list");
$val1->set_list ("int", 1, 2, 3);
@list = $val1->get_list ();
print $list[0] == 1 &&  $list[1] == 2 && $list[2] == 3 ? "ok 10\n" : "not ok 10\n";

$val1 = new GConf::Value ("list", "int", 1, 2, 3);
@list = $val1->get_list ();
print $list[0] == 1 &&  $list[1] == 2 && $list[2] == 3 ? "ok 11\n" : "not ok 11\n";

$val1 = new GConf::Value ("list");
$val1->set_list ("string", "blah", "de", "blah");
@list = $val1->get_list ();
print $list[0] eq "blah" &&  $list[1] eq "de" && $list[2] eq "blah" ? "ok 12\n" : "not ok 12\n";

$val1 = new GConf::Value ("list", "string", "blah", "de", "blah");
@list = $val1->get_list ();
print $list[0] eq "blah" &&  $list[1] eq "de" && $list[2] eq "blah" ? "ok 13\n" : "not ok 13\n";

print $val1->get_list_type () eq "string" ? "ok 14\n" : "not ok 14\n";

$val1 = new GConf::Value ("pair");
$val1->set_pair ("string", "blah", "int", 10);
@pair = $val1->get_pair ();
print $pair[0] eq "blah" &&  $pair[1] == 10 ? "ok 15\n" : "not ok 15\n";

$val1 = new GConf::Value ("pair", "string", "blah", "int", 10);
@pair = $val1->get_pair ();
print $pair[0] eq "blah" &&  $pair[1] == 10 ? "ok 16\n" : "not ok 16\n";

print $val1->get_type () eq "pair" ? "ok 17\n" : "not ok 17\n";

$val2 = $val1->get_car ();
print $val2->stringify () eq "blah" ? "ok 18\n" : "not ok 18\n";

$val2 = $val1->get_cdr ();
print $val2->stringify () eq "10" ? "ok 19\n" : "not ok 19\n";
