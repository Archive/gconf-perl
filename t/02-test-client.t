#!/usr/bin/perl -w

BEGIN { $| = 1; print "1..10\n\n"; }
END {print "not ok  1\n" unless $loaded;}

use strict;
use vars qw($loaded);
use ExtUtils::testlib;
use GConf;

$loaded = 1;
print "ok  1\n";

my $client = new GConf::Client ();
my $error;
my ($val1, $val2);
my @list;

$client->set_int ("/tmp/test-int", 111, $error);
print !$error ? "ok  2\n" : "not ok  2\n";
print $client->get_int ("/tmp/test-int", $error) == 111 ? "ok  3\n" : "not ok  3\n";
print !$error ? "ok  4\n" : "not ok  4\n";

$val1 = new GConf::Value ("int", 111);
$client->set ("/tmp/test-value", $val1, $error);
print !$error ? "ok  5\n" : "not ok  5\n";

$val2 = $client->get ("/tmp/test-value", $error);
print !$error ? "ok  6\n" : "not ok  6\n";

print $val2->get_type () eq "int" ? "ok  7\n" : "not ok  7\n";
print $val2->get_int () == 111 ? "ok  8\n" : "not ok  8\n";

@list = $client->all_dirs ("/", $error);
print !$error && @list ? "ok  9\n" : "not ok  9\n";

@list = $client->all_entries ("/tmp", $error);
print !$error && @list ? "ok 10\n" : "not ok 10\n";
